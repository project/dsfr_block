<?php
/**
 * @file
 * Contains \Drupal\dsfr_block\Form\CreatetypeblocksForm.
 */
namespace Drupal\dsfr_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class CreateTypeBlockForm extends FormBase {

  /**
   * @return string
   */
  public function getFormId()  {

    return 'dsfr_block_type_create';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // What we want to record
    $blocks_type = \Drupal::service('dsfr_block.blockStorage')->typeBlocks();
    // Existing types of blocks
    $type_names = \Drupal::service('dsfr_block.blockManage')->getBlockTypes();
    // Checkbox disabled or not
    $options_attributes = [];

    // $list = \Drupal::service('dsfr_block.blockManage')->getListBlockTypesDesired();
    // dd($list);
    // die;

    // Create checkboxes
    foreach ( $blocks_type as $key => $row ) {
      $options[$key] = $row['label'];
      if( in_array( $key, $type_names )) {
        
        $options_attributes[$key]['disabled'] = 'disabled';
      } 
    }

    // form
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['block_checkboxes'] = [
      '#title' => $this->t('Select the type blocks to create:'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#options_attributes' => $options_attributes
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $options = $form_state->getValue('block_checkboxes');
    
    \Drupal::service('dsfr_block.blockManage')->createBlockTypes( $options );
  
    $route_name = 'entity.block_content_type.collection'; 
    $route_parameters = $options = []; 
    $url_object = Url::fromRoute($route_name, $route_parameters, $options);
    $url_string = $url_object->toString();

    $build = [
      '#type' => 'container',
      '#markup' => t('<a href=":url">' . $this->t('Type of blocks'). '</a> ' .t('created') .'!', [':url' => $url_string]),
      '#allowed_tags' => ['a'],
    ];
    $result = \Drupal::service('renderer')->renderPlain($build);
    $this->messenger()->addStatus($result);
  }
}