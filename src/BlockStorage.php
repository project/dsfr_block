<?php

namespace Drupal\dsfr_block;

use Drupal\Core\Controller\ControllerBase;

class BlockStorage extends ControllerBase
{

  /**
   * {@inheritdoc}
   */
  public function typeBlocks()
  {

    return $blocks_type = [

      'block_alert' => [

        'label' => $this->t('Alert Block'),
        'desc' => $this->t("Alerts can be used to draw the user's attention to a piece of information without interrupting the task in hand."),
        'fields' => [
          'field_margin_top',
          'field_alert',
          'field_compact',
          'field_title',
          'field_message',
          'field_close',
          'field_margin_bottom'
        ]
      ],
      'block_callout' => [

        'label' => $this->t('Callout Block'),
        'desc' => $this->t("Highlighting allows the user to quickly distinguish information that complements the content consulted."),
        'fields' => [
          'field_margin_top',
          'field_color',
          'field_title',
          //'field_message',
          //'field_action',
          'field_margin_bottom'
        ]
      ],
      'block_highlight' => [

        'label' => $this->t('Highlight Block'),
        'desc' => $this->t("Highlighting allows the user to distinguish and locate information easily."),
        'fields' => [
          'field_margin_top',
          'field_color',
          'field_font_size',
          //'field_message',
          'field_margin_bottom'
        ]
      ],
      'block_link' => [

        'label' => $this->t('Link Block'),
        'desc' => $this->t("The link allows navigation between a page and another content within the same page, the same site or external. The link must be explicit."),
        'fields' => [
          'field_margin_top',
          //'field_position',
          'field_action',
          'field_margin_bottom'
        ]
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldsBlockNotice()
  {
    return [

      'description' => 'Data for DSFR Notice',
      'fields' =>  [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'message' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'date_start' => [
          'type' => 'varchar',
          'mysql_type' => 'datetime',
          'not null' => FALSE,
        ],
        'date_end' => [
          'type' => 'varchar',
          'mysql_type' => 'datetime',
          'not null' => FALSE,
        ],
        'close' => [
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'default' => 0,
        ],
      ],
      'primary key' => ['id'], 
    ];
  }
}